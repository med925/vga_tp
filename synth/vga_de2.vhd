library ieee;
use ieee.std_logic_1164.all;

entity vga_DE2 is
	port(
		CLOCK_50 :IN std_logic;		--	50 MHz
		reset:IN std_logic;		-- reset key
		SW: IN std_logic_vector (9 downto 0);-- switches
		VGA_CLK:OUT std_logic;		-- 
		VGA_HS: OUT std_logic;		-- VGA H_SYNC
		VGA_VS:OUT std_logic;		-- VGA V_SYNC
		VGA_BLANK:OUT std_logic;		-- VGA BLANK
		VGA_SYNC:OUT std_logic;		-- VGA SYNC
		VGA_R: OUT std_logic_vector(9 downto 0);-- VGA Red[9:0]
		VGA_G: OUT std_logic_vector(9 downto 0);-- VGA Green[9:0]
		VGA_B: OUT std_logic_vector(9 downto 0)	-- VGA Blue[9:0]
	);
end entity;

architecture arch_vga_DE2 of vga_DE2 is

component vga21 is 
port(
	clk, rst: in std_logic;
	hs,vs,blank,sync,clk_out : out std_logic;
	r,g,b : in std_logic_vector(9 downto 0);
	r_out,g_out,b_out : out std_logic_vector(9 downto 0)
	
);
end component;

signal clk_25: std_logic;
signal r,g,b : std_logic_vector (9 downto 0);
begin
	process(CLOCK_50)
	begin
		if(CLOCK_50'event and CLOCK_50='1') then 
			if(reset = '0') then 
				clk_25<='0';
			else
				clk_25 <= not clk_25;
			end if;
		end if;
	end process;
	
	r<=SW(9 downto 6) & "111111";
	g<=SW(5 downto 3) & "1111111"; 
	b<=SW(2 downto 0) & "1111111";
	
	vga_assignement: VGA21 port map(
		clk=>clk_25, 
		rst=> not reset,	
		hs=>VGA_HS,
		vs=>VGA_VS,
		blank=>VGA_BLANK,
		sync=>VGA_SYNC,
		clk_out=>VGA_CLK,
		r=>r,
		g=>g,
		b=>b,
		r_out=>VGA_R,
		g_out=>VGA_G,
		b_out=>VGA_B
	);
	
end arch_vga_DE2;

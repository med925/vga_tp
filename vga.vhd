library ieee;
use ieee.std_logic_1164.all;

	Entity vga21 is 
	port(
		clk, rst: in std_logic;
		hs,vs,blank,sync,clk_out : out std_logic;
		r,g,b : in std_logic_vector(9 downto 0);
		r_out,g_out,b_out : out std_logic_vector(9 downto 0)
		
	);
	end entity;

architecture vga21_arch of vga21 is
--	Horizontal Parameter	( Pixel )
constant 	H_SYNC_CYC : integer	:=	96;
constant 	H_SYNC_BACK : integer	:=	48;
constant 	H_SYNC_ACT : integer	:=	640;
constant 	H_SYNC_FRONT : integer	:=	16;
constant 	H_SYNC_TOTAL : integer	:=	800;
--	Virtical Parameter		( Line )
constant 	V_SYNC_CYC : integer	:=	2;
constant 	V_SYNC_BACK : integer	:=	32;
constant 	V_SYNC_ACT : integer	:=	480;
constant 	V_SYNC_FRONT : integer	:=	11;
constant 	V_SYNC_TOTAL : integer	:=	525;
--	Start Offset
constant 	X_START : integer		:=	H_SYNC_CYC+H_SYNC_BACK;
constant 	Y_START : integer		:=	V_SYNC_CYC+V_SYNC_BACK;

signal x_count, y_count: integer range 0 to 1023;
signal blank_s, hs_s, vs_s: std_logic;
begin
	process(clk)
	begin
		if (clk='1') then 
			if(rst='1') then
				x_count<=0;
				y_count<=0;
			else
				if(x_count=799) then 
					x_count<=0;
					if(y_count=524) then 
						y_count<=0;
					else
						y_count<=y_count+1;
					end if;
				else
					x_count<=x_count+1;
				end if;
			end if;
		end if;
	end process;
	
	hs_s<='0' when x_count< H_SYNC_FRONT else '1';
	vs_s<='0' when y_count< V_SYNC_FRONT else '1';
	blank_s<='1' when 
			x_count>=X_START and x_count<X_START+H_SYNC_ACT and 
			y_count>=Y_START and y_count<Y_START+V_SYNC_ACT
		else '0';
	sync<=hs_s and vs_s;
	vs<=vs_s;
	hs<=hs_s;
	blank<=blank_s;
	clk_out<=clk;
	r_out<=r when blank_s='1' else (others=>'0');
	g_out<=g when blank_s='1' else (others=>'0');
	b_out<=b when blank_s='1' else (others=>'0');
	
	
	
end vga21_arch;
